package service;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ICollectionService {
    List<String> splitTextIntoWords(String text);

    Map<String, Long> getWordsOccurrences(String text);

    Set<String> getUniqueWords(String text);

    List<String> sortWords(String text);
}
