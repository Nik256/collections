package service;

import java.util.*;

public class CollectionService implements ICollectionService {
    @Override
    public List<String> splitTextIntoWords(String text) {
        return Arrays.asList(text.split("\\W+"));
    }

    @Override
    public Map<String, Long> getWordsOccurrences(String text) {
        Map<String, Long> occurrences = new HashMap<>();
        List<String> words = splitTextIntoWords(text);
        for (String word : words) {
            Long count = occurrences.get(word);
            if (count == null)
                count = 0L;
            occurrences.put(word, ++count);
        }
        return occurrences;
    }

    @Override
    public Set<String> getUniqueWords(String text) {
        return new HashSet<>(splitTextIntoWords(text));
    }

    @Override
    public List<String> sortWords(String text) {
        List<String> list = new ArrayList<>(getUniqueWords(text));
        list.sort((s1, s2) -> s1.length() == s2.length() ? s1.compareToIgnoreCase(s2) : Integer.compare(s1.length(), s2.length()));
        return list;
    }
}
