package service;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StreamCollectionService implements ICollectionService {
    @Override
    public List<String> splitTextIntoWords(String text) {
        return Pattern.compile("\\W+").splitAsStream(text).collect(Collectors.toList());
    }

    @Override
    public Map<String, Long> getWordsOccurrences(String text) {
        return splitTextIntoWords(text).stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    @Override
    public Set<String> getUniqueWords(String text) {
        return splitTextIntoWords(text).stream().collect(Collectors.toSet());
    }

    @Override
    public List<String> sortWords(String text) {
        return getUniqueWords(text).stream().sorted(Comparator.comparing(String::length).thenComparing(String::compareToIgnoreCase)).
                collect(Collectors.toList());
    }
}
